#include "deck.h"

/*
	The Player Class
	This is an abstract class that contains a set of functions required by
	both the user(s) and the AI. Has a vector called m_cards that stores the cards of the player.
	Has standard functions to print, add, and remove cards.
*/


// Returns the blackjack value of a card (i.e., if given 0 returns 1, if given 1 returns 2, if given 10-12 returns 10)
unsigned int blackjackValue(int num);

class Player {
public:
	// Constructors for the Player class
	Player::Player();
	Player::Player(string name);

	// Prints all the cards that a Player has
	void printCards();

	// Adds a card to the Player's vector
	void addCard(Card c);

	// Clears all the cards that a Player has
	void clearPlayerCards();

	// Returns the blackjack sum (i.e., J, Q, K equal 10, Ace is one, and so on)
	unsigned int blackjackSum();

	// Checks for Blackjack
	bool checkBlackjack();

	// Checks for bust
	bool checkBust();

	// A pure virtual function. Returns the move that the player makes at a given situation.
	virtual string setPlayerMove() = 0;

	string moves[6] = { "hit", "stand", "blackjack", "bust", "null", "rerun" };

	string returnName() { return m_name; }

protected:
	vector<Card> m_cards;
	string m_name;
};

class User : public Player {
public:
	// User constructor.
	User::User();

	// Sets the name of the User.
	void setPlayerName();

	// Function inherited from Player. Asks for input from the user.
	string setPlayerMove();
};

class AI : public Player {
public:
	// AI constructor
	AI::AI();

	// Function inherited from Player. Calculates computer's move.
	// Algorithm: This is just a basic probability calculator. 
	// I.E. if sum is between 0 and 11, the computer will definitely hit
	// if sum is between 12 and 15, there is a 8 in 13 chance the computer will hit
	// if sum is between 16 and 18, there is a 4 in 13 chance the computer will hit
	// if sum is between 18 and 20, there is a 1 in 13 chance the computer will hit
	// Note: This assumes that int rand() returns a perfectly random number
	// Note: The values 7, 3, and 0 are stored in namespace ai_pr so that they can be called
	// and changed with ease through this function
	string setPlayerMove();

};