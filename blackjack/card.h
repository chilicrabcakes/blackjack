#pragma once
#include <iostream>
#include <vector>
#include <string>
using namespace std;

/*
	The Card Class
	Represents a single card in a 52-card deck. Each card has a suit and a number.
	In this class, a suit is represented by an array of suits.
	A number represents the type of the card, i.e., a two or a King. 
	This is done by using numbers from 0-12. 0 is an Ace, 1 is Two, etc. 
	10 is Jack, 11 is Queen, and 12 is King. 
*/
class Card{
public:
	// Public standard constructor, initializes values
	Card::Card(int s, int n);

	// Prints the suit and number of the Card
	void printType();

	// Returns the suit (as an enum) of the Card
	int returnSuit();

	// Returns the number (where 0 is Ace, 10 is Jack) of the Card
	int returnNumber();

private:
	string suit[4] = { "Spades", "Hearts", "Clubs", "Diamonds" };
	int m_suit;
	int m_num;
};

// This function converts num values to printable string values. 
// i.e. takes the value 0 and prints an Ace, or takes 10 and prints Jack.
// Implementation: Using a switch-case statement.
string numberConverter(int num);

