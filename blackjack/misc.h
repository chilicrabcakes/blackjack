


// Exception class

class NumException : public exception {
public:
	const char * what() const throw () {
		return "number exception - numbers beyond acceptable ranges";
	}

};

