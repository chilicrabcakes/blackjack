#include "player.h"

/*
	The Blackjack Class
	This is the main game class, which contains the main run function.
*/

class Blackjack {
public:
	Blackjack();

	// The main game function. Contains main runtime loop which calls all other functions and classes.
	void run();

	// Prints out a start message.
	void printStartMessage();

	// Deals two cards to both User and AI
	void deal();

	// Prints out both user and AI cards.
	void printCards();

private:
	User m_user;
	AI m_ai;
	Deck m_deck;
	bool m_rungame;

};
