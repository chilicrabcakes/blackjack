#include "card.h"

/*
	The Deck Class
	This is basically a wrapper for a vector containing all possible 52 cards.
	Contains functions such as:
	createNewDeck, which generates all 52 cards in order and pushes them into the vector.
	shuffleDeck, which shuffles the deck using the std::random_shuffle function.
	pickCard, which pops a card from the top of the deck.
*/

class Deck {
public:
	// Constructor for Deck. Calls the createNewDeck and shuffleDeck function
	Deck::Deck();

	// Generates a standard 52-card deck in order from spades to diamonds, ace to king
	void createNewDeck();

	// Shuffles the cards in the vector around, using std::random_shuffle
	void shuffleDeck();

	// Picks the top card from the shuffled card, and returns that card. Pops that card
	// from the deck. In the event that the deck is empty before picking the card, generates
	// a new deck. 
	Card pickCard();

private:
	vector<Card> m_deck;
};