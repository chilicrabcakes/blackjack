#include "blackjack.h"
#include "misc.h"

/*  //////////////////////////////////////////////////////////////////////////////////////////////
	BLACKJACK CLASS FUNCTION DEFINITIONS
*/  //////////////////////////////////////////////////////////////////////////////////////////////

Blackjack::Blackjack() : m_rungame(true) {
	run();
}

void Blackjack::run() {
	printStartMessage();
	deal();

	string s;
	string p;

	while (m_rungame) {
		printCards();

		// User's moves
		if (s != "stand") s = m_user.setPlayerMove();

		if (s == "null") {
			cout << "Something went wrong with the program. Please contact administrator." << endl;
			return;
		}

		if (s == "hit") {
			cout << m_user.returnName() << " chose to hit!" << endl;
			m_user.addCard(m_deck.pickCard());
		}
		else if (s == "blackjack") {
			cout << "Blackjack!" << m_user.returnName() << " wins!" << endl;
			m_rungame = false;
		}
		else if (s == "bust") {
			cout << m_user.returnName() << " is bust! Dealer wins!" << endl;
			m_rungame = false;
		}
		else if (s == "rerun") {
			continue;
		}
	

		// Dealer's moves
		if (p != "stand") p = m_ai.setPlayerMove();

		if (p == "null") {
			cout << "Something went wrong with the program. Please contact administrator." << endl;
			return;
		}

		if (p == "hit") {
			cout << "Dealer chose to hit!" << endl;
			m_ai.addCard(m_deck.pickCard());
		}
		else if (p == "blackjack") {
			cout << "Blackjack!" << " Dealer wins!" << endl;
			m_rungame = false;
		}
		else if (s == "bust") {
			cout << "Dealer is bust!" << m_user.returnName() << "wins!" << endl;
			m_rungame = false;
		}
	
		if (p == "stand" && s == "stand") {
			cout << "Both have stood! Time to check for victory!" << endl;
			if (m_user.blackjackSum() > m_ai.blackjackSum()) {
				cout << m_user.returnName() << " wins!" << endl;
			}
			else if (m_user.blackjackSum() == m_ai.blackjackSum()) {
				cout << "Draw!" << endl;
			}
			else {
				cout << "Dealer Wins!" << endl;
			}
			return;
		}

	
	}
	
}

void Blackjack::printStartMessage() {
	cout << "Welcome to Blackjack! Let's play! \n" << endl;
	m_user.setPlayerName();
}

void Blackjack::deal() {
	cout << "Dealing cards...\n" << endl;

	// Adds a card to each user
	m_user.addCard(m_deck.pickCard());
	m_ai.addCard(m_deck.pickCard());
	m_user.addCard(m_deck.pickCard());
	m_ai.addCard(m_deck.pickCard());

}

void Blackjack::printCards() {
	
	cout << m_user.returnName() << "'s cards are: " << endl;
	m_user.printCards();
	cout << endl;
	cout << "Dealer's cards are: " << endl;
	m_ai.printCards();
}
