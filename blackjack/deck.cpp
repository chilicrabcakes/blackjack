#include "deck.h"
#include "misc.h"

NumException num1;

// These includes are required for std::random_shuffle and std::shuffle
#include <ctime>        // std::time
#include <cstdlib>      // std::rand, std::srand
#include <algorithm>
#include <iterator>

Deck::Deck() {
	createNewDeck();
	shuffleDeck();
}

// Implementation: This function will run a for loop inside a for loop, for suits and numbers. 
void Deck::createNewDeck() {

	m_deck.clear();

	for (int i = 0; i <= 3; i++) {
		for (int j = 0; j <= 12; j++) {
			m_deck.push_back(Card(i, j));
		}
	}
}

void Deck::shuffleDeck() {
	if (m_deck.size() == 0) return;
	std::random_shuffle(m_deck.begin(), m_deck.end());
}

Card Deck::pickCard() {
	if (m_deck.size() == 0) {
		cout << "Deck has finished! Generating New Deck... " << endl;
		createNewDeck();
		shuffleDeck();
	}

	Card c = m_deck.back();
	m_deck.pop_back();
	return c;
}

// Note: Is there a better way to shuffle a deck than by using std::random_shuffle?
// Note: How do I deal with empty decks?