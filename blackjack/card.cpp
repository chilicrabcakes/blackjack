#include "card.h"
#include "misc.h"

NumException numex;

string numberConverter(int num) {

	try {
		if (num >= 0 && num <= 12) {}
		else throw numex;
	}
	catch (NumException e) { cout << e.what() << endl; }

	switch (num)
	{
	case 0:
		return "Ace";
		break;
	case 1:
		return "Two";
		break;
	case 2:
		return "Three";
		break;
	case 3:
		return "Four";
		break;
	case 4:
		return "Five";
		break;
	case 5:
		return "Six";
		break;
	case 6:
		return "Seven";
		break;
	case 7:
		return "Eight";
		break;
	case 8:
		return "Nine";
		break;
	case 9:
		return "Ten";
		break;
	case 10:
		return "Jack";
		break;
	case 11:
		return "Queen";
		break;
	case 12:
		return "King";
		break;
	default:
		return ""; //Shouldn't reach this point, due to the Exception
		break;
	}
}

Card::Card(int s, int n){
	//cout << "-HI from constructor" << endl;
	try {
		if (s >= 0 && s <= 3 && n >= 0 && n <= 12) {} // I'm a little rusty at C++ programming, sorry
		else throw numex;
	}
	catch (NumException e) {
		cout << e.what() << endl;
		return;
	}

	m_suit = s;
	m_num = n;
}

void Card::printType() {
	cout << numberConverter(m_num) << " of " << suit[m_suit] << endl;
}

int Card::returnNumber() { return m_num; }
int Card::returnSuit() { return m_suit; }
