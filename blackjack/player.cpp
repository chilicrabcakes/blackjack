#include "player.h"
#include "misc.h"

// Contains the probability values for the AI (check AI->setPlayerMove() for more information)
namespace ai_pr {
	int LOW_X = 7;
	int MID_X = 3;
	int HIGH_X = 0;
	int Y = 13;
};

NumException num2;

unsigned int blackjackValue(int num) {
	try {
		if (num >= 0 && num <= 12) {}
		else throw num2;
	}
	catch (NumException e) {
		cout << "blackjackValue: " << e.what() << endl;
	}

	if (num >= 0 && num <= 9) {
		return num + 1;
	}

	return 10;
}

/*  //////////////////////////////////////////////////////////////////////////////////////////////
	PLAYER CLASS FUNCTION DEFINITIONS
*/  //////////////////////////////////////////////////////////////////////////////////////////////

Player::Player() : m_name("") {}

Player::Player(string name) : m_name(name) {}

void Player::printCards() {
	if (m_cards.size() == 0) {
		cout << "No cards to print!" << endl;
		return;
	}

	for (unsigned int i = 0; i < m_cards.size(); i++) {
		m_cards[i].printType();
	}
	return;
}

void Player::addCard(Card c) {
	m_cards.push_back(c);
}

void Player::clearPlayerCards() {
	if (m_cards.size() == 0) return;
	m_cards.clear();
}

unsigned int Player::blackjackSum() {
	int sum = 0;
	for (unsigned int i = 0; i < m_cards.size(); i++) {
		sum += blackjackValue(m_cards[i].returnNumber());
	}
	return sum;
}

bool Player::checkBlackjack() {
	if (this->blackjackSum() == 21) return true;

	return false;
}

bool Player::checkBust() {
	if (this->blackjackSum() > 21) return true;

	return false;
}

/*  //////////////////////////////////////////////////////////////////////////////////////////////
	User Class Function Definitions
*/  //////////////////////////////////////////////////////////////////////////////////////////////

User::User() : Player() {
}

// Prompts User to provide a name 
void User::setPlayerName() {
	cout << "What is your name? " << endl;

	string s;
	cin >> s;

	m_name = s;
}

// Note: I understand hard-coding responses is not great programming style, but I think
// it's easier and quicker to do it this way than to use some function like toLower or toUpper to 
// take care of all possibilities. In addition, it does not require me to include extra files to 
// my program.
string User::setPlayerMove() {
	if (checkBlackjack()) return moves[2];
	if (checkBust()) return moves[3];

	cout << returnName() << "'s turn. Hit? Please type in yes for hit, or no for stand." << endl;

	string s;
	cin >> s;

	if (s == "y" || s == "Y" || s == "yes" || s == "Yes" || s == "YES" ||
		s == "h" || s == "H" || s == "hit" || s == "Hit" || s == "HIT") {
		return moves[0]; // Returns "hit"
	}
	else if (s == "n" || s == "N" || s == "no" || s == "No" || s == "YES" ||
		     s == "s" || s == "S" || s == "stand" || s == "Stand" || s == "STAND") {
		return moves[1]; // Returns "stand"
	}

	cout << "We didn't quite get your move. Please try again. " << endl;
	return moves[5]; //Returns "rerun" - this should prompt the main game loop to rerun getPlayerMove.
}

/*  //////////////////////////////////////////////////////////////////////////////////////////////
    AI Class Function Definitions
*/  //////////////////////////////////////////////////////////////////////////////////////////////

AI::AI() : Player("Dealer") {}

string AI::setPlayerMove() {
	if (checkBlackjack()) return moves[2];
	if (checkBust()) return moves[3];

	unsigned int sum = blackjackSum();
	int rand_x = rand() % ai_pr::Y; // 0 to 12

	if (sum <= 11) return moves[0]; // Returns "hit"
	else if (sum <= 15) {
		if (rand_x <= ai_pr::LOW_X) return moves[0];
		else return moves[1];
	}
	else if (sum <= 18) {
		if (rand_x <= ai_pr::MID_X) return moves[0];
		else return moves[1];
	}
	else if (sum <= 20) {
		if (rand_x <= ai_pr::HIGH_X) return moves[0];
		else return moves[1];
	}
	
	return moves[4]; // null value return
}