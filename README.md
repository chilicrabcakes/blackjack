# README #

This is a basic (non-GUI, just text) application that allows the user to play a game of blackjack with the computer.

Blackjack is a simple n-player card game involving a dealer and a player (or players). It is a standard game played in casinos across the world. 
For more information on the game itself, please check https://en.wikipedia.org/wiki/Blackjack. 

### What is this repository for? ###

This repository holds C++ code for the Blackjack program.
Version 1.0 (not complete as of yet)

### How do I get set up? ###

Make sure you have some form of C++ compiler (and/or IDE). Ex: gcc, g++, clang++, etc. 
If you are running Visual Studio, you can clone straight into the IDE.

Other IDE's not supported as of yet. Please follow the steps below:
Clone the git project.
Compile the C++ files using your C++ compiler in command line 
(i.e. gcc -o <all-the-files> blackjack)
Run the executable 
(i.e. ./blackjack)

### Who do I talk to? ###

Repo Owner: Ayush Lall (username: chilicrabcakes)
Contact: ayushlall@g.ucla.edu

### Notes ###
As of now, the game treats the value of an Ace as 1. Will implement the either 1-or-11 in next version.
Version 1.0 not yet complete - bugs are still being worked out of the program!